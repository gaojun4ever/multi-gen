ROOT_DIR=
python3 ${ROOT_DIR}/inference.py \
    -gpuid 0 \
    -test_data /home/xiapeng/gaojun/trans/test/nist02.cn \
    -test_out ./test_out \
    -model ./out_dir/checkpoint_epoch0.pkl \
    -vocab ./demo.vocab.pt \ 
    -config ./config.yml \
    -beam_size 3 \
    -decode_max_length 100
