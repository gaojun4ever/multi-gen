import torch
import torch.nn as nn
from torch.nn import functional
from torch.autograd import Variable
from kgdlg.Trainer import Statistics,PriorStatistics
import kgdlg.IO
import torch.nn.functional as F


class NMTLossCompute(nn.Module):
    """
    Standard NMT Loss Computation.
    """
    def __init__(self, generator, tgt_vocab):
        super(NMTLossCompute, self).__init__()
        self.generator = generator
        self.tgt_vocab = tgt_vocab
        self.padding_idx = tgt_vocab.stoi[kgdlg.IO.PAD_WORD]
        weight = torch.ones(len(tgt_vocab))
        weight[self.padding_idx] = 0
        weight[kgdlg.IO.UNK] = 0
        self.criterion = nn.NLLLoss(weight, size_average=False)

    def make_shard_state(self, batch, output):
        """ See base class for args description. """
        return {
            "output": output,
            "target": batch.tgt[1:],
            }   

    def compute_loss(self, batch, output, target):

        scores = self.generator(self.bottle(output))
        target = target.view(-1)
        loss = self.criterion(scores,target)

        loss_data = loss.item()
        stats = self.stats(loss_data, scores, target)
        return  loss, stats

    def compute_train_loss(self, batch, output):
        """
        Compute the loss in shards for efficiency.
        """
        batch_stats = Statistics()
        shard_state = self.make_shard_state(batch, output)
        loss, stats = self.compute_loss(batch, **shard_state)
        loss.div(batch.batch_size).backward()
        batch_stats.update(stats)

        return batch_stats       
        
    def compute_valid_loss(self, batch, output):
        """
        Compute the loss monolithically, not dividing into shards.
        """

        shard_state = self.make_shard_state(batch, output)
        _, batch_stats = self.compute_loss(batch, **shard_state)

        return batch_stats

    def stats(self, loss, scores, target):
        """
        Compute and return a Statistics object.
        Args:
            loss(Tensor): the loss computed by the loss criterion.
            scores(Tensor): a sequence of predict output with scores.
        """
        pred = scores.max(1)[1]
        non_padding = target.ne(self.padding_idx)
        num_correct = pred.eq(target) \
                          .masked_select(non_padding) \
                          .sum()
        return Statistics(loss, non_padding.sum().item(), num_correct.item())

    def bottle(self, v):
        return v.view(-1, v.size(2))

    def unbottle(self, v, batch_size):
        return v.view(-1, batch_size, v.size(1)) 


class PriorLossCompute(nn.Module):
    """
    Standard NMT Loss Computation.
    """
    def __init__(self, num_clusters, tgt_vocab):
        super(PriorLossCompute, self).__init__()
        self.tgt_vocab = tgt_vocab
        self.padding_idx = tgt_vocab.stoi[kgdlg.IO.PAD_WORD]
        weight = torch.ones(len(tgt_vocab))
        weight[self.padding_idx] = 0
        weight[kgdlg.IO.UNK] = 0
        self.word_crt = nn.NLLLoss(weight, size_average=True)
        weight = torch.ones(num_clusters)
        weight[0] = 0
        self.cluster_crt = nn.NLLLoss(size_average=True)
    def compute_loss(self, batch, cluster_logits, word_logits):

        cluster_targets = batch.cluster.transpose(0,1)
        keyword_targets = batch.keyword[0]
        cluster_logits = self.bottle(cluster_logits)
        cluster_logits = F.log_softmax(cluster_logits,-1)
        cluster_targets = cluster_targets.view(-1)
        cluster_loss = self.cluster_crt(cluster_logits,cluster_targets)
        
        word_logits = self.bottle(word_logits)
        word_logits = F.log_softmax(word_logits,-1)
        keyword_targets = keyword_targets.view(-1)

        word_loss = self.word_crt(word_logits, keyword_targets)

        loss = cluster_loss+word_loss
        loss.backward()
        cluster_loss_data = cluster_loss.item()
        word_loss_data = word_loss.item()
        stats = PriorStatistics(cluster_loss_data,word_loss_data)
        return  stats

    def bottle(self, v):
        return v.view(-1, v.size(2))

    def unbottle(self, v, batch_size):
        return v.view(-1, batch_size, v.size(1)) 


class JointLossCompute(nn.Module):
    """
    Standard NMT Loss Computation.
    """
    def __init__(self, generator, tgt_vocab):
        super(JointLossCompute, self).__init__()
        self.generator = generator
        self.tgt_vocab = tgt_vocab
        self.padding_idx = tgt_vocab.stoi[kgdlg.IO.PAD_WORD]
        weight = torch.ones(len(tgt_vocab))
        weight[self.padding_idx] = 0
        weight[kgdlg.IO.UNK] = 0
        self.criterion = nn.NLLLoss(weight, size_average=False)
        # self.kld = nn.KLDivLoss(size_average=False)
    def compute_loss(self, batch, output, kld_loss, crossentropy_loss):
        target = batch.tgt[0][1:]
        scores = self.generator(self.bottle(output))
        target = target.view(-1)
        seq_loss = self.criterion(scores,target)

        loss = seq_loss+kld_loss+crossentropy_loss
        loss.div(batch.batch_size).backward()
        seq_loss_data = seq_loss.item()
        kld_loss_data = kld_loss.item()/batch.batch_size
        stats = self.stats(seq_loss_data, kld_loss_data, scores, target)
        return  stats
    def stats(self, seq_loss, kld_loss, scores, target):
        """
        Compute and return a Statistics object.
        Args:
            loss(Tensor): the loss computed by the loss criterion.
            scores(Tensor): a sequence of predict output with scores.
        """
        pred = scores.max(1)[1]
        non_padding = target.ne(self.padding_idx)
        num_correct = pred.eq(target) \
                          .masked_select(non_padding) \
                          .sum()
        return Statistics(seq_loss, kld_loss, non_padding.sum().item(), num_correct.item())

    def bottle(self, v):
        return v.view(-1, v.size(2))

    def unbottle(self, v, batch_size):
        return v.view(-1, batch_size, v.size(1)) 