import torch
import torch.nn as nn
from kgdlg.modules.Attention import GlobalAttention,KeywordAttention
# from nmt.modules.SRU import SRU
from kgdlg.modules.StackedRNN import StackedGRU, StackedLSTM
import torch.nn.functional as F
from torch.autograd import Variable
import math
import random     


            


class AttnDecoder(nn.Module):
    """ The GlobalAttention-based RNN decoder. """
    def __init__(self, rnn_type, embedding, attn_type, input_size, 
                hidden_size, num_layers=1, dropout=0.1):
        super(AttnDecoder, self).__init__()        
        # Basic attributes.
        self.rnn_type = rnn_type
        self.embedding = embedding
        self.attn_type = attn_type
        self.num_layers = num_layers
        self.hidden_size = hidden_size
        self.dropout = nn.Dropout(dropout)  

        self.rnn = getattr(nn, rnn_type)(
                input_size=input_size,
                hidden_size=hidden_size,
                num_layers=num_layers,
                dropout=dropout)              

        if self.attn_type != 'none':
            self.attn = GlobalAttention(hidden_size, attn_type)

    def forward(self, input, context, state):
        emb = self.embedding(input)
        rnn_outputs, hidden = self.rnn(emb, state)
        

        if self.attn_type != 'none':
            # Calculate the attention.
            attn_outputs, attn_scores = self.attn(
                rnn_outputs.transpose(0, 1).contiguous(),  # (output_len, batch, d)
                context.transpose(0, 1)                   # (contxt_len, batch, d)
            )

            outputs  = self.dropout(attn_outputs)    # (input_len, batch, d)
            attn = attn_outputs
        else:
            outputs  = self.dropout(rnn_outputs)
            attn = None

        return outputs , hidden, attn

class KeyAttnDecoder(nn.Module):
    """ The GlobalAttention-based RNN decoder. """
    def __init__(self, rnn_type, embedding, attn_type, input_size, 
                hidden_size, num_layers=1, dropout=0.1):
        super(KeyAttnDecoder, self).__init__()        
        # Basic attributes.
        self.rnn_type = rnn_type
        self.attn_type = attn_type
        self.num_layers = num_layers
        self.hidden_size = hidden_size
        self.dropout = nn.Dropout(dropout)  
        self.embedding = embedding
        self.rnn = getattr(nn, rnn_type)(
                input_size=input_size,
                hidden_size=hidden_size,
                num_layers=num_layers,
                dropout=dropout)              

        if self.attn_type != 'none':
            self.attn = KeywordAttention(hidden_size, attn_type)

    def forward(self, input, context, state, latent_z):
        emb = self.embedding(input)
        rnn_outputs, hidden = self.rnn(emb, state)

        if self.attn_type != 'none':
            # Calculate the attention.
            attn_outputs, attn_scores = self.attn(
                rnn_outputs.transpose(0, 1).contiguous(),  # (output_len, batch, d)
                context.transpose(0, 1),                   # (contxt_len, batch, d)
                latent_z.transpose(0, 1)
            )

            outputs  = self.dropout(attn_outputs)    # (input_len, batch, d)
            attn = attn_outputs
        else:
            outputs  = self.dropout(rnn_outputs)
            attn = None

        return outputs , hidden, attn
