from kgdlg.modules.Encoder import BasicEncoder
from kgdlg.modules.Decoder import AttnDecoder,KeyAttnDecoder
from kgdlg.modules.Embedding import Embedding,ClusterEmbedding
from kgdlg.Model import NMTModel,PriorNet,RecogNet,MultiGen
import torch
import torch.nn as nn
import kgdlg.IO as IO




# def create_emb_for_encoder_and_decoder(src_vocab_size,
#                                        tgt_vocab_size,
#                                        src_embed_size,
#                                        tgt_embed_size,
#                                        padding_idx):

#     embedding_encoder = Embedding(src_vocab_size,src_embed_size,padding_idx)
#     embedding_decoder = Embedding(tgt_vocab_size,tgt_embed_size,padding_idx)

        
#     return embedding_encoder, embedding_decoder

def create_encoder(opt, embedding):
    
    rnn_type = opt.rnn_type
    input_size = opt.embedding_size
    hidden_size = opt.hidden_size
    num_layers = opt.num_layers
    dropout = opt.dropout
    bidirectional = opt.bidirectional

    encoder = BasicEncoder(rnn_type,
                        embedding,
                        input_size,
                        hidden_size,
                        num_layers,
                        dropout,
                        bidirectional)

    return encoder

def create_decoder(opt,embedding):

    decoder_type = opt.decoder_type
    rnn_type = opt.rnn_type  
    atten_model = opt.atten_model
    input_size = opt.embedding_size
    hidden_size = opt.hidden_size
    num_layers = opt.num_layers
    dropout = opt.dropout 

    if decoder_type == 'AttnDecoder':
        decoder = KeyAttnDecoder(rnn_type,
                                embedding,
                                atten_model,
                                input_size,
                                hidden_size,
                                num_layers,
                                dropout)


    return decoder

def create_generator(input_size, output_size):
    generator = nn.Sequential(
        nn.Linear(input_size, output_size),
        nn.LogSoftmax(dim=-1))
    return generator


def create_prior_model(opt, fields):
    vocab_size = len(fields['tgt'].vocab)
    padding_idx = fields['tgt'].vocab.stoi[IO.PAD_WORD]
    embedding_size = opt.embedding_size
    embedding = Embedding(vocab_size,embedding_size, padding_idx)

    rnn_type = opt.rnn_type
    input_size = opt.embedding_size
    hidden_size = opt.hidden_size
    num_layers = opt.num_layers
    dropout = opt.dropout
    bidirectional = opt.bidirectional
    encoder = BasicEncoder(rnn_type,
                        embedding,
                        input_size,
                        hidden_size,
                        num_layers,
                        dropout,
                        bidirectional)
    cluster_embedding = ClusterEmbedding(opt.num_clusters+1, embedding_size)

    prior_net = PriorNet(encoder, cluster_embedding, vocab_size)

    return prior_net


def create_recog_model(opt, fields):
    vocab_size = len(fields['tgt'].vocab)
    padding_idx = fields['tgt'].vocab.stoi[IO.PAD_WORD]
    embedding_size = opt.embedding_size
    embedding = Embedding(vocab_size,embedding_size, padding_idx)

    rnn_type = opt.rnn_type
    input_size = opt.embedding_size
    hidden_size = opt.hidden_size
    num_layers = opt.num_layers
    dropout = opt.dropout
    bidirectional = opt.bidirectional
    src_encoder = BasicEncoder(rnn_type,
                        embedding,
                        input_size,
                        hidden_size,
                        num_layers,
                        dropout,
                        bidirectional)
    tgt_encoder = BasicEncoder(rnn_type,
                        embedding,
                        input_size,
                        hidden_size,
                        num_layers,
                        dropout,
                        bidirectional)
    cluster_embedding = ClusterEmbedding(opt.num_clusters+1, embedding_size)

    recog_net = RecogNet(src_encoder, tgt_encoder, cluster_embedding, vocab_size)

    return recog_net


def create_joint_model(opt, fields, cluster_dict):
    vocab_size = len(fields['tgt'].vocab)
    padding_idx = fields['tgt'].vocab.stoi[IO.PAD_WORD]
    embedding_size = opt.embedding_size
    embedding = Embedding(vocab_size,embedding_size, padding_idx)

    prior_net = create_prior_model(opt, fields)
    recog_net = create_recog_model(opt, fields)
    encoder = create_encoder(opt, embedding)
    decoder = create_decoder(opt,embedding)
    generator = create_generator(opt.hidden_size, vocab_size)
    cluster_embedding = ClusterEmbedding(opt.num_clusters+1, embedding_size)

    mask_set = set(list(range(vocab_size)))
    joint_model = MultiGen(prior_net, recog_net, 
                            encoder, decoder, generator,
                            cluster_embedding, mask_set, cluster_dict)

    return joint_model

# def create_base_model(opt, fields):
#     src_vocab_size = len(fields['src'].vocab)
#     tgt_vocab_size = len(fields['tgt'].vocab)
#     padding_idx = fields['src'].vocab.stoi[IO.PAD_WORD]
#     enc_embedding, dec_embedding = \
#             create_emb_for_encoder_and_decoder(src_vocab_size,
#                                                 tgt_vocab_size,
#                                                 opt.embedding_size,
#                                                 opt.embedding_size,
#                                                 padding_idx)
#     encoder = create_encoder(opt)
#     decoder = create_decoder(opt)
#     generator = create_generator(opt.hidden_size, tgt_vocab_size)
#     model = NMTModel(enc_embedding, 
#                      dec_embedding, 
#                      encoder, 
#                      decoder, 
#                      generator)

#     # model.apply(weights_init)
#     return model

