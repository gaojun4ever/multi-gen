import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
class PriorNet(nn.Module):
    def __init__(self, encoder, cluster_embedding, num_keywords):
        super(PriorNet, self).__init__()
        self.encoder = encoder
        self.cluster_embedding = cluster_embedding
        self.num_clusters = cluster_embedding.num_clusters
        self.num_keywords = num_keywords
        # self.linear0 = nn.Linear(cluster_embedding.embedding_size, self.encoder.hidden_size*2)
        self.linear1 = nn.Linear(self.encoder.hidden_size*2,
                                        cluster_embedding.num_clusters)
        self.fc_out = nn.Linear(self.encoder.hidden_size*2+cluster_embedding.embedding_size, \
                                        num_keywords)
    def forward(self, src_inputs, lengths, cls_target):
        enc_outputs, enc_hidden = self.encode(src_inputs, lengths)
        cluster_logits = self.linear1(enc_hidden)
        cls_embeded = self.cluster_embedding(cls_target)
        cls_embeded = cls_embeded.transpose(0,1)
        latent = torch.cat([enc_hidden,cls_embeded],-1)
        keyword_logits = self.fc_out(latent)
        return cluster_logits, keyword_logits
    def sample_cluster(self, src_inputs, lengths, cluster_dict, mask_set):
        enc_outputs, enc_hidden = self.encode(src_inputs, lengths)
        cluster_logits = self.linear1(enc_hidden)
        raw_cluster_logits = cluster_logits.clone()
        cluster_logits[:,:,0]=float("-inf")
        # print(cluster_logits.size())
        cluster_p = F.softmax(cluster_logits,-1).squeeze(0)
        selected_cluster = []
        selected_cluster_p = []
        for p in cluster_p.tolist():
            selected_idx = np.random.choice(self.num_clusters, 1, p)
            
            selected_cluster.append(selected_idx)
            selected_cluster_p.append([p[selected_idx[0]]])
        selected_cluster = torch.LongTensor(selected_cluster)
        if src_inputs.is_cuda:
            selected_cluster = selected_cluster.cuda()
        cls_embeded = self.cluster_embedding(selected_cluster.transpose(0,1))
        # cls_embeded = cls_embeded
        latent = torch.cat([enc_hidden,cls_embeded],-1)
        keyword_logits = self.fc_out(latent)  
        raw_keyword_logits = keyword_logits.clone()
        mask_indices = self.get_keyword_mask(selected_cluster,cluster_dict, mask_set)
        for i,m in enumerate(mask_indices):
            keyword_logits[0][i][m] = float("-inf")
        keyword_p = F.softmax(keyword_logits,-1).squeeze()
        selected_keyword = [np.random.choice(self.num_keywords, 1, p) for p in keyword_p.tolist()]
        selected_keyword = torch.LongTensor(selected_keyword)
        if src_inputs.is_cuda:
            selected_keyword = selected_keyword.cuda()

        return selected_cluster, selected_keyword, cls_embeded, selected_cluster_p, raw_keyword_logits,raw_cluster_logits
    def get_topk(self, src_inputs, lengths, cluster_dict, mask_set, k):
        enc_outputs, enc_hidden = self.encode(src_inputs, lengths)
        cluster_logits = self.linear1(enc_hidden)
        cluster_logits[:,:,0]=float("-inf")
        cluster_logits = cluster_logits.squeeze()
        # print(cluster_logits.size())
        selected_cluster_logits, selected_cluster = cluster_logits.topk(10,-1)
        selected_cluster = selected_cluster[k].view(1,-1)
        # cluster_p = F.softmax(cluster_logits,-1).squeeze()
        # selected_cluster = []
        # selected_cluster_p = []
        # for p in cluster_p.tolist():
        #     selected_idx = np.random.choice(self.num_clusters, 1, p)
            
        #     selected_cluster.append(selected_idx)
        #     selected_cluster_p.append([p[selected_idx[0]]])
        # selected_cluster = torch.LongTensor(selected_cluster.unsqueeze(0))
        # if src_inputs.is_cuda:
        #     selected_cluster = selected_cluster.cuda()
        cls_embeded = self.cluster_embedding(selected_cluster)
        # cls_embeded = cls_embeded

        latent = torch.cat([enc_hidden,cls_embeded],-1)
        keyword_logits = self.fc_out(latent)  
        
        mask_indices = self.get_keyword_mask(selected_cluster,cluster_dict, mask_set)
        for i,m in enumerate(mask_indices):
            keyword_logits[0][i][m] = float("-inf")
        # keyword_p = F.softmax(keyword_logits,-1).squeeze()
        selected_keyword_logits, selected_keyword = keyword_logits.topk(1,-1)

        # selected_keyword = torch.LongTensor(selected_keyword)
        # if src_inputs.is_cuda:
        #     selected_keyword = selected_keyword.cuda()

        return selected_cluster, selected_keyword, cls_embeded
    def get_keyword_mask(self, cls_target, cluster_dict, mask_set):
        
        cls_target = cls_target[:,0].tolist()
        mask_indices = []
        for c in cls_target:
            if c != 0:
                mask_indices.append(list(mask_set-set(cluster_dict[str(c)])))
            else:
                mask_indices.append([])
        return mask_indices
    def encode(self, inputs, lengths):
        order_inputs, sorted_length, sorted_idx =self.order_sequence(inputs,lengths)
        enc_outputs, enc_hidden = self.encoder(order_inputs, sorted_length, None)
        enc_hidden = self.reorder_enc_seqence(enc_hidden,sorted_idx)
        enc_outputs = self.reorder_enc_seqence(enc_outputs,sorted_idx)
        return enc_outputs, enc_hidden
    def order_sequence(self, inputs, lengths):
        idx_len_pair = []
        for i,l in enumerate(lengths):
            idx_len_pair.append((i,l))
        sorted_idx_len_pair=sorted(idx_len_pair,key=lambda x:x[1],reverse=True)
        sorted_idx = []
        sorted_length = []
        for x in sorted_idx_len_pair:
            sorted_idx.append(x[0])
            sorted_length.append(x[1])

        order_inputs = inputs[:,sorted_idx]
        return order_inputs, sorted_length, sorted_idx
    def reorder_enc_seqence(self, inputs, sorted_idx):
        raw_sorted_pair = []
        for raw_idx,sorted_idx in enumerate(sorted_idx):
            raw_sorted_pair.append((raw_idx,sorted_idx))
        sorted_pair=sorted(raw_sorted_pair,key=lambda x:x[1],reverse=False)
        raw_indices = [x[0] for x in sorted_pair]
        reorder_inputs = inputs[:,raw_indices,:]
        return reorder_inputs

    def save_checkpoint(self, epoch, opt, filename):
        torch.save({'model_dict': self.state_dict(),
                    'opt': opt,
                    'epoch': epoch,
                    },
                   filename)

    def load_checkpoint(self, filename):   
        ckpt = torch.load(filename,map_location=lambda storage, loc: storage)
        self.load_state_dict(ckpt['model_dict'])
        epoch = ckpt['epoch']
        return epoch


class RecogNet(nn.Module):
    def __init__(self, src_encoder, tgt_encoder, cluster_embedding, num_keywords):
        super(RecogNet, self).__init__()
        self.src_encoder = src_encoder
        self.tgt_encoder = tgt_encoder
        self.num_clusters = cluster_embedding.num_clusters
        self.num_keywords = num_keywords
        self.cluster_embedding = cluster_embedding
        self.linear1 = nn.Linear(self.src_encoder.hidden_size*2*2,
                                        cluster_embedding.num_clusters)
        self.fc_out = nn.Linear(self.src_encoder.hidden_size*2*2+cluster_embedding.embedding_size, \
                                        num_keywords)
        

                    
    def forward(self, src_inputs, src_lengths,
                        tgt_inputs, tgt_legnths,
                        cls_target):
        src_enc_outputs, src_enc_hidden = self.src_encode(src_inputs, src_lengths)
        tgt_enc_outputs, tgt_enc_hidden = self.tgt_encode(tgt_inputs, tgt_legnths)
        src_tgt_hidden = torch.cat([src_enc_hidden,tgt_enc_hidden],-1)
        cluster_logits = self.linear1(src_tgt_hidden)
        cls_embeded = self.cluster_embedding(cls_target)
        cls_embeded = cls_embeded.transpose(0,1)
        latent = torch.cat([src_tgt_hidden,cls_embeded],-1)
        keyword_logits = self.fc_out(latent)       
        return cluster_logits, keyword_logits

    def src_encode(self, inputs, lengths):
        order_inputs, sorted_length, sorted_idx =self.order_sequence(inputs,lengths)
        enc_outputs, enc_hidden = self.src_encoder(order_inputs, sorted_length, None)
        enc_hidden = self.reorder_enc_seqence(enc_hidden,sorted_idx)
        enc_outputs = self.reorder_enc_seqence(enc_outputs,sorted_idx)
        return enc_outputs, enc_hidden
    def tgt_encode(self, inputs, lengths):
        order_inputs, sorted_length, sorted_idx =self.order_sequence(inputs,lengths)
        enc_outputs, enc_hidden = self.tgt_encoder(order_inputs, sorted_length, None)
        enc_hidden = self.reorder_enc_seqence(enc_hidden,sorted_idx)
        enc_outputs = self.reorder_enc_seqence(enc_outputs,sorted_idx)
        return enc_outputs, enc_hidden     
    def order_sequence(self, inputs, lengths):
        idx_len_pair = []
        for i,l in enumerate(lengths):
            idx_len_pair.append((i,l))
        sorted_idx_len_pair=sorted(idx_len_pair,key=lambda x:x[1],reverse=True)
        sorted_idx = []
        sorted_length = []
        for x in sorted_idx_len_pair:
            sorted_idx.append(x[0])
            sorted_length.append(x[1])

        order_inputs = inputs[:,sorted_idx]
        return order_inputs, sorted_length, sorted_idx
    def reorder_enc_seqence(self, inputs, sorted_idx):
        raw_sorted_pair = []
        for raw_idx,sorted_idx in enumerate(sorted_idx):
            raw_sorted_pair.append((raw_idx,sorted_idx))
        sorted_pair=sorted(raw_sorted_pair,key=lambda x:x[1],reverse=False)
        raw_indices = [x[0] for x in sorted_pair]
        reorder_inputs = inputs[:,raw_indices,:]
        return reorder_inputs

    def sample_cluster(self, src_inputs, src_lengths,
                        tgt_inputs, tgt_legnths, cluster_dict, mask_set):
        src_enc_outputs, src_enc_hidden = self.src_encode(src_inputs, src_lengths)
        tgt_enc_outputs, tgt_enc_hidden = self.tgt_encode(tgt_inputs, tgt_legnths)
        src_tgt_hidden = torch.cat([src_enc_hidden,tgt_enc_hidden],-1)
        cluster_logits = self.linear1(src_tgt_hidden)
        raw_cluster_logits = cluster_logits.clone()

        cluster_logits[:,:,0]=float("-inf")
        cluster_p = F.softmax(cluster_logits,-1).squeeze()
        selected_cluster = []
        selected_cluster_p = []
        for p in cluster_p.tolist():
            selected_idx = np.random.choice(self.num_clusters, 1, p)
            selected_cluster.append(selected_idx)
            selected_cluster_p.append([p[selected_idx[0]]])
        # selected_cluster = [np.random.choice(self.num_clusters, 1, p) for p in cluster_p.tolist()]
        selected_cluster = torch.LongTensor(selected_cluster)
        if src_inputs.is_cuda:
            selected_cluster = selected_cluster.cuda()

        cls_embeded = self.cluster_embedding(selected_cluster.transpose(0,1))
        # cls_embeded = cls_embeded.transpose(0,1)
        latent = torch.cat([src_tgt_hidden,cls_embeded],-1)
        keyword_logits = self.fc_out(latent)  
        raw_keyword_logits = keyword_logits.clone()
        mask_indices = self.get_keyword_mask(selected_cluster, cluster_dict, mask_set)
        for i,m in enumerate(mask_indices):
            keyword_logits[0][i][m] = float("-inf")
        keyword_p = F.softmax(keyword_logits,-1).squeeze()
        selected_keyword = [np.random.choice(self.num_keywords, 1, p) for p in keyword_p.tolist()]
        selected_keyword = torch.LongTensor(selected_keyword)
        if src_inputs.is_cuda:
            selected_keyword = selected_keyword.cuda()
        return selected_cluster, selected_keyword, cls_embeded, selected_cluster_p, raw_keyword_logits,raw_cluster_logits

    def get_keyword_mask(self, cls_target, cluster_dict, mask_set):
        
        cls_target = cls_target[:,0].tolist()
        mask_indices = []
        for c in cls_target:
            if c != 0:
                mask_indices.append(list(mask_set-set(cluster_dict[str(c)])))
            else:
                mask_indices.append([])

        return mask_indices
    def save_checkpoint(self, epoch, opt, filename):
        torch.save({'model_dict': self.state_dict(),
                    'opt': opt,
                    'epoch': epoch,
                    },
                   filename)

    def load_checkpoint(self, filename):   
        ckpt = torch.load(filename,map_location=lambda storage, loc: storage)
        self.load_state_dict(ckpt['model_dict'])
        epoch = ckpt['epoch']
        return epoch

class MultiGen(nn.Module):
    def __init__(self, prior_net, recog_net,
                        encoder, decoder, generator,
                        cluster_embedding, mask_set, cluster_dict):
        super(MultiGen, self).__init__()
        self.prior_net = prior_net
        self.recog_net = recog_net
        self.encoder = encoder
        self.decoder = decoder
        self.generator = generator
        self.cluster_embedding = cluster_embedding
        self.cluster_dict = cluster_dict
        self.mask_set = mask_set
        self.fc_z = nn.Linear(self.encoder.embedding.embedding_size*2,self.decoder.hidden_size)
    def forward(self, src_inputs, src_lengths,
                        tgt_inputs, tgt_legnths, cls_target):

        prior_selected_cluster, prior_selected_keyword, prior_cls_embeded, \
                    prior_selected_cluster_p, prior_keyword_logits, prior_cluster_logits= self.prior_net.sample_cluster(src_inputs, src_lengths,self.cluster_dict, self.mask_set)
        recog_selected_cluster, recog_selected_keyword, recog_cls_embeded, \
                    recog_selected_cluster_p, recog_keyword_logits, recog_cluster_logits= self.recog_net.sample_cluster(src_inputs, src_lengths,
                        tgt_inputs, tgt_legnths, self.cluster_dict, self.mask_set)
        recog_keyword_embs = self.encoder.embedding(recog_selected_keyword)
        recog_keyword_embs = recog_keyword_embs.transpose(0,1)
        latent_z_input = torch.cat([recog_keyword_embs,recog_cls_embeded],-1)
        latent_z = self.fc_z(latent_z_input)

        enc_outputs, enc_hidden = self.encode(src_inputs, src_lengths)
        dec_init_hidden = enc_hidden
        dec_outputs , dec_hiddens, attn = self.decoder(
                tgt_inputs, enc_outputs, dec_init_hidden, latent_z
            )
        prior_selected_cluster_p = torch.FloatTensor(prior_selected_cluster_p)
        recog_selected_cluster_p = torch.FloatTensor(recog_selected_cluster_p)
        
        prior_keyword_p = F.softmax(prior_keyword_logits,-1)
        recog_keyword_p = F.softmax(recog_keyword_logits,-1)
        if src_inputs.is_cuda:
            prior_selected_cluster_p = prior_selected_cluster_p.cuda()
            recog_selected_cluster_p = recog_selected_cluster_p.cuda()
        prior_p = prior_keyword_p
        recog_p = recog_keyword_p

        kld_loss = recog_p*(recog_p.log()-prior_p.log())
        kld_loss = kld_loss.view(-1,1).sum()
        prior_selected_keyword = prior_selected_keyword.squeeze()
        prior_selected_cluster = prior_selected_cluster.squeeze()

     

        prior_cluster_loss = F.cross_entropy(prior_cluster_logits.squeeze(), prior_selected_cluster.view(-1), size_average=False)
        recog_cluster_loss = F.cross_entropy(recog_cluster_logits.squeeze(), recog_selected_cluster.view(-1), size_average=False)

        prior_keyword_loss = F.cross_entropy(prior_keyword_logits.squeeze(), prior_selected_keyword.view(-1), size_average=False)
        recog_keyword_loss = F.cross_entropy(recog_keyword_logits.squeeze(), recog_selected_keyword.view(-1), size_average=False)

        crossentropy_loss = prior_cluster_loss+recog_cluster_loss+prior_keyword_loss+recog_keyword_loss
        # log_prior_cluster = F.log_softmax(prior_cluster_logits,-1)
        # log_recog_cluster = F.log_softmax(recog_cluster_logits,-1)

        # print(log_prior_cluster.size())
        # print(prior_selected_cluster)
        # print(prior_selected_cluster.size())
        # raise BaseException("break")
        # print(log_prior_cluster[:,:,prior_selected_cluster])
        # print(log_prior_cluster[:,:,prior_selected_cluster].size())
        

        # log_prior_keyword = prior_keyword_p[prior_selected_keyword].log()
        # log_recog_keyword = recog_keyword_p[recog_selected_keyword].log()
        return dec_outputs, kld_loss, crossentropy_loss
    def get_topk(self, src_inputs, src_lengths, k):
        selected_cluster, selected_keyword, cls_embeded \
                = self.prior_net.get_topk(src_inputs, src_lengths,self.cluster_dict, self.mask_set, k)
        return selected_cluster, selected_keyword, cls_embeded

    def encode(self, src_inputs, src_lengths):
        
        order_inputs, sorted_length, sorted_idx =self.order_sequence(src_inputs,src_lengths)
        enc_outputs, enc_hidden = self.encoder(order_inputs, sorted_length, None)
        enc_hidden = self.reorder_enc_seqence(enc_hidden,sorted_idx)
        enc_outputs = self.reorder_enc_seqence(enc_outputs,sorted_idx)
        return enc_outputs, enc_hidden
    def get_keyword_mask(self, cls_target):
        
        cls_target = cls_target[:,0].tolist()
        mask_indices = []
        for c in cls_target:
            
            mask_indices.append(list(self.mask_set-set(self.cluster_dict[str(c)])))

        return mask_indices


    def order_sequence(self, inputs, lengths):
        idx_len_pair = []
        for i,l in enumerate(lengths):
            idx_len_pair.append((i,l))
        sorted_idx_len_pair=sorted(idx_len_pair,key=lambda x:x[1],reverse=True)
        sorted_idx = []
        sorted_length = []
        for x in sorted_idx_len_pair:
            sorted_idx.append(x[0])
            sorted_length.append(x[1])

        order_inputs = inputs[:,sorted_idx]
        return order_inputs, sorted_length, sorted_idx
    def reorder_enc_seqence(self, inputs, sorted_idx):
        raw_sorted_pair = []
        for raw_idx,sorted_idx in enumerate(sorted_idx):
            raw_sorted_pair.append((raw_idx,sorted_idx))
        sorted_pair=sorted(raw_sorted_pair,key=lambda x:x[1],reverse=False)
        raw_indices = [x[0] for x in sorted_pair]
        reorder_inputs = inputs[:,raw_indices,:]
        return reorder_inputs
    def save_checkpoint(self, epoch, opt, filename):
        torch.save({'model_dict': self.state_dict(),
                    'opt': opt,
                    'epoch': epoch,
                    },
                   filename)

    def load_checkpoint(self, filename):   
        ckpt = torch.load(filename,map_location=lambda storage, loc: storage)
        self.load_state_dict(ckpt['model_dict'])
        epoch = ckpt['epoch']
        return epoch
class NMTModel(nn.Module):
    def __init__(self, enc_embedding, dec_embedding, encoder, decoder, generator):
        super(NMTModel, self).__init__()
        self.enc_embedding = enc_embedding
        self.dec_embedding = dec_embedding
        self.encoder = encoder
        self.decoder = decoder
        self.generator = generator

    def forward(self, src_inputs, tgt_inputs, src_lengths):

        # Run wrods through encoder

        enc_outputs, enc_hidden = self.encode(src_inputs, src_lengths, None)


        dec_init_hidden = self.init_decoder_state(enc_hidden, enc_outputs)
            
        dec_outputs , dec_hiddens, attn = self.decode(
                tgt_inputs, enc_outputs, dec_init_hidden
            )        

        return dec_outputs, attn



    def encode(self, input, lengths=None, hidden=None):
        emb = self.enc_embedding(input)
        enc_outputs, enc_hidden = self.encoder(emb, lengths, None)

        return enc_outputs, enc_hidden

    def init_decoder_state(self, enc_hidden, context):
        return enc_hidden

    def decode(self, input, context, state):
        emb = self.dec_embedding(input)
        dec_outputs , dec_hiddens, attn = self.decoder(
                emb, context, state
            )     

        return dec_outputs, dec_hiddens, attn
    
    def save_checkpoint(self, epoch, opt, filename):
        torch.save({'encoder_dict': self.encoder.state_dict(),
                    'decoder_dict': self.decoder.state_dict(),
                    'enc_embedding_dict': self.enc_embedding.state_dict(),
                    'dec_embedding_dict': self.dec_embedding.state_dict(),
                    'generator_dict': self.generator.state_dict(),
                    'opt': opt,
                    'epoch': epoch,
                    },
                   filename)

    def load_checkpoint(self, filename):   
        ckpt = torch.load(filename)
        self.enc_embedding.load_state_dict(ckpt['enc_embedding_dict'])
        self.dec_embedding.load_state_dict(ckpt['dec_embedding_dict'])
        self.encoder.load_state_dict(ckpt['encoder_dict'])
        self.decoder.load_state_dict(ckpt['decoder_dict'])
        self.generator.load_state_dict(ckpt['generator_dict'])
        epoch = ckpt['epoch']
        return epoch